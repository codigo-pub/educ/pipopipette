﻿namespace Pipopipette
{
    public class Game
    {
        //Gestionnaire d'ecoute pour que le parent souscrive 
        //le type delegate 'NotifyScoreDelegate' est declare dans le parent avant la classe
        public event NotifyScoreDelegate? NotifyScoreEvent;

        public int scorePlayer1 { get; private set; }
        public int scorePlayer2 { get; private set; }

        public Game()
        {
        }

        public void NotifyScore()
        {
            if (NotifyScoreEvent != null)
            {
                //Raise Event. All the listeners of this event will get a call.               
                NotifyScoreEvent();
            }
        }

        /// <summary>
        /// met a jour le score des deux joueurs
        /// </summary>
        private void updateScore()
        {
                //ici tu pourras mettre a jour le score des joueurs

                //Notifie via les events souscris que le score a potentiellement ete mis a jour
                this.NotifyScore();
        }

    }
}
