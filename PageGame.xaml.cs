﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Pipopipette
{

    //Cree un delegate pour la notification des scores
    public delegate void NotifyScoreDelegate();

    /// <summary>
    /// Logique d'interaction pour PageGame.xaml
    /// </summary>
    public partial class PageGame : Page
    {

        private Game? game;

        public PageGame()
        {
            InitializeComponent();
        }

        private void _canvas_Loaded(object sender, RoutedEventArgs e)
        {
            game = new Game();
            //enregistre event notification
            game.NotifyScoreEvent += new NotifyScoreDelegate(this.scoreUpdatedEvent);
        }

        public void scoreUpdatedEvent()
        {
            Debug.WriteLine("score mis a jour ?!");
            if (game != null)
            {
                this.scorePlayer1.Content = game.scorePlayer1;
                this.scorePlayer2.Content = game.scorePlayer2;
            }
        }
    }
}
